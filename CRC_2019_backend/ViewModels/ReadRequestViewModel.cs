﻿
namespace CRC_2019_backend.ViewModels
{
    public class ReadRequestViewModel : BaseViewModel
    {       
        public string ServerName { get; set; }
        
        public string ServerAddress { get; set; }
        
        public string Permission { get; set; }  
        
        public string Status { get; set; }   
        
        public string AdditionalInfo { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }
    }
}
