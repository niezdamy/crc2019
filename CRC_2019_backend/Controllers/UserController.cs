﻿using System.Collections.Generic;
using CRC_2019_backend.Persistence.Models;
using CRC_2019_backend.Services;
using CRC_2019_backend.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CRC_2019_backend.Controllers
{
    [AllowAnonymous]
    [Route("api/user")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IRoleService _roleService;
        private readonly IUserService _userService;

        public UserController(IUserService userService, IRoleService roleService)
        {
            _userService = userService;
            _roleService = roleService;
        }

        [HttpGet("all")]
        public IEnumerable<UserViewModel> GetAllUsers()
        {
            return _userService.GetAllUsers();
        }

        [HttpGet("UserRoles/{login}")]
        public RoleSimply GetUserRoles(string login)
        {
            return _roleService.GetUserRoles(login);
        }
    }
}
