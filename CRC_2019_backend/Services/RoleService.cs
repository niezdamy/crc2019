﻿using CRC_2019_backend.Persistence;
using CRC_2019_backend.Persistence.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CRC_2019_backend.Services
{
    public class RoleService: IRoleService
    {
        private readonly IGenericRepository<RoleSimply> _roleRepository;

        public RoleService(IGenericRepository<RoleSimply> roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public RoleSimply GetUserRoles(string login)
        {
            return _roleRepository.GetAll().Include(y => y.Roles).FirstOrDefault(x => x.Login == login);
        }
    }
}
