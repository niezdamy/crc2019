﻿using CRC_2019_backend.Repository.Enums;
using System.ComponentModel.DataAnnotations;

namespace CRC_2019_backend.ViewModels
{
    public class CreateRequestViewModel: BaseViewModel
    {
        [Required]
        public ServersEnum ServerName { get; set; }

        [Required]
        [StringLength(200)]
        public string ServerAddress { get; set; }

        [Required]
        public PermissionsEnum Permission { get; set; }

        public int UserId { get; set; }       

        [StringLength(500)]
        public string AdditionalInfo { get; set; }
    }
}
