﻿using CRC_2019_backend.Services;
using CRC_2019_backend.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CRC_2019_backend.Controllers
{
    [AllowAnonymous]
    [Route("api/request")]
    [ApiController]
    public class RequestController : BaseCrcController
    {
        private readonly IRequestService _requestService;

        public RequestController(IRequestService requestService)
        {
            _requestService = requestService;
        }

        [HttpGet("all")]
        public ActionResult<IEnumerable<ReadRequestViewModel>> GetAllRequests()
        {
            return ExecuteAction(() => _requestService.GetAllIRequests());
        }

        [HttpGet("forUser/{id}")]
        public ActionResult<IEnumerable<ReadRequestViewModel>> GetUserRequests(int id)
        {
            return ExecuteAction(() => _requestService.GetMyRequests(id));
        }

        [HttpPost("create")]
        public ActionResult<int> CreateRequest([FromBody]CreateRequestViewModel request)
        {
            return ExecuteAction(() => _requestService.CreateNewRequest(request));
        }

        [HttpPut("approve/{id}")]
        public IActionResult Approve(int id)
        {
            return ExecuteAction(() => _requestService.Approve(id));
        }

        [HttpPut("reject/{id}")]
        public IActionResult Reject(int id)
        {
            return ExecuteAction(() => _requestService.Reject(id));
        }

        [HttpDelete("delete/{id}")]
        public IActionResult Delete(int id)
        {
            return ExecuteAction(() => _requestService.Delete(id));
        }
    }
}
