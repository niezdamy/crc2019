﻿
namespace CRC_2019_backend.ViewModels
{
    public class UserViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public string Login { get; set; }  
        
        public bool IsLogin { get; set; }
    }
}
