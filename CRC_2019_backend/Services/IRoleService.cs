﻿using CRC_2019_backend.Persistence.Models;

namespace CRC_2019_backend.Services
{
    public interface IRoleService
    {
        RoleSimply GetUserRoles(string login);
    }
}