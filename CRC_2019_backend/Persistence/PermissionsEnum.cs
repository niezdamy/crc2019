﻿using System.ComponentModel;

namespace CRC_2019_backend.Repository.Enums
{
    public enum PermissionsEnum
    {
        [Description("Read only")]
        ReadOnly,
        [Description("Admin")]
        Admin,
        [Description("HPA")]
        Hpa
    }
}