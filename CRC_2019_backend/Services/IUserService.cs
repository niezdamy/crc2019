﻿using CRC_2019_backend.ViewModels;
using System.Collections.Generic;

namespace CRC_2019_backend.Services
{
    public interface IUserService
    {
        IEnumerable<UserViewModel> GetAllUsers();

        UserViewModel GetUserByLogin(string login);

        UserViewModel GetUserById(int id);

        int LogInUser(string name, string password);

        bool LogOutUser(int id);

        void DeleteUser(int id);
    }
}
