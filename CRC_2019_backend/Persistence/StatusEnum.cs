﻿using System.ComponentModel;

namespace CRC_2019_backend.Repository.Enums
{
    public enum StatusEnum
    {
        [Description("In progress")]
        InProgress,
        [Description("Approved")]
        Approved,
        [Description("Rejected")]
        Rejected,       
    }
}
