﻿using System.ComponentModel;

namespace CRC_2019_backend.Repository.Enums
{
    public enum ServersEnum
    {
        [Description("Development")]
        Dev,
        [Description("Test")]
        Tst,
        [Description("Acceptance")]
        Acc,
        [Description("Production")]
        Prd
    }
}
