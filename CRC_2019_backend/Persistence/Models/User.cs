﻿namespace CRC_2019_backend.Persistence.Models
{
    public class User : Entity
    {
        public string Name { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public bool IsLogin { get; set; }
    }
}